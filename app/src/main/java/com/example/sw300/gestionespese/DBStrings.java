package com.example.sw300.gestionespese;

/**
 * Created by sw300 on 12/05/16.
 */
public class DBStrings {

    public static final String DB_NAME  = "spesa_db";
    public static final int DB_VER      = 5;

    public static final String TABLE_SPESA              = "spesa";
    public static final String FIELD_ID_TABLE_SPESA     = "_id";
    public static final String FIELD_DESCRIZIONE        = "descrizione";
    public static final String FIELD_PREZZO             = "prezzo";
    public static final String FIELD_CATEGORIA          = "id_categoria";

    public static final String TABLE_CATEGORIA          = "categoria";
    public static final String FIELD_ID_TABLE_CATEGORIA = "_id";
    public static final String FIELD_NOME               = "nome";
}
