package com.example.sw300.gestionespese;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;

import static com.example.sw300.gestionespese.DBStrings.*;

/**
 * Created by _void__ on 12/05/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    static final String SQL_CREATE_TABLE_SPESA = "CREATE TABLE " + TABLE_SPESA + " ("
            + FIELD_ID_TABLE_SPESA + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FIELD_DESCRIZIONE + " TEXT, "
            + FIELD_PREZZO + " INTEGER, "
            + FIELD_CATEGORIA + " TEXT)";

    static final String SQL_CREATE_TABLE_CATEGORIA = "CREATE TABLE " + TABLE_CATEGORIA + " ("
            + FIELD_ID_TABLE_CATEGORIA + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FIELD_NOME + " TEXT UNIQUE ON CONFLICT IGNORE)";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate( SQLiteDatabase db ) {

        System.out.println( "Database creato" );

    /* creo le tabelle */

        System.out.println( "Inizio creazione tabelle..." );

        createTable( db, SQL_CREATE_TABLE_SPESA );
        System.out.println("\tTabella '" + TABLE_SPESA + "' creata con successo;");

        createTable( db, SQL_CREATE_TABLE_CATEGORIA );
        System.out.println("\tTabella '" + TABLE_CATEGORIA + "' creata con successo;");

        System.out.println("Creazione tabelle completata.");

    /* popolo le tabelle con dati fittizi */

        /* popolo la tabella categoria */

        ContentValues[] records = new ContentValues[5];

        //INSERT INTO categoria (nome) VALUES('Senza categoria')
        records[0] = new ContentValues(); records[0].put( FIELD_NOME, "Senza categoria" );

        //INSERT INTO categoria (nome) VALUES('Casa')
        records[1] = new ContentValues(); records[1].put( FIELD_NOME, "Casa" );

        //INSERT INTO categoria (nome) VALUES('Cultura')
        records[2] = new ContentValues(); records[2].put( FIELD_NOME, "Cultura" );

        //INSERT INTO categoria (nome) VALUES('Salute')
        records[3] = new ContentValues(); records[3].put( FIELD_NOME, "Salute" );

        //INSERT INTO categoria (nome) VALUES('Tempo libero')
        records[4] = new ContentValues(); records[4].put( FIELD_NOME, "Tempo libero" );

        HashMap<String, Long> id = popolaTabella( db, TABLE_CATEGORIA, records );

        /* popolo la tabella spesa*/

        /*Cursor cursor = db.query( TABLE_CATEGORIA, null, null, null, null, null, null);
        int id_senza_categoria = cursor.getColumnIndex( "Senza categoria" );
        int id_casa = cursor.getColumnIndex( "Casa" );
        //int id_cultura = cursor.getColumnIndex( "Cultura" );
        //int id_salute = cursor.getColumnIndex( "Salute" );
        int id_tempo_libero = cursor.getColumnIndex( "Tempo libero" );
        cursor.close();*/

        records = new ContentValues[6];

        //INSERT INTO spesa (desrizione, prezzo, categoria) VALUES('Lavatrice', 29900, id_casa)
        records[0] = new ContentValues();
        records[0].put( FIELD_DESCRIZIONE, "Lavatrice");
        records[0].put( FIELD_PREZZO, 29900);
        records[0].put( FIELD_CATEGORIA, id.get("Casa").intValue());

        //INSERT INTO spesa (desrizione, prezzo, categoria) VALUES('Aria fritta', 2950, id_senza_categoria)
        records[1] = new ContentValues();
        records[1].put( FIELD_DESCRIZIONE, "Aria fritta");
        records[1].put( FIELD_PREZZO, 2950);
        records[1].put( FIELD_CATEGORIA, id.get("Senza categoria").intValue());

        //INSERT INTO spesa (desrizione, prezzo, categoria) VALUES('Corso android', 0, id_tempo_libero)
        records[2] = new ContentValues();
        records[2].put( FIELD_DESCRIZIONE, "Corso android");
        records[2].put( FIELD_PREZZO, 0);
        records[2].put( FIELD_CATEGORIA, id.get("Tempo libero").intValue());

        //INSERT INTO spesa (desrizione, prezzo, categoria) VALUES('Acqua bagnata', 12075, id_senza_categoria)
        records[3] = new ContentValues();
        records[3].put( FIELD_DESCRIZIONE, "Acqua bagnata");
        records[3].put( FIELD_PREZZO, 12075);
        records[3].put( FIELD_CATEGORIA, id.get("Senza categoria").intValue());

        //INSERT INTO spesa (desrizione, prezzo, categoria) VALUES('Fuoco di paglia', 35, id_senza_categoria)
        records[4] = new ContentValues();
        records[4].put( FIELD_DESCRIZIONE, "Fuoco di paglia");
        records[4].put( FIELD_PREZZO, 35);
        records[4].put( FIELD_CATEGORIA, id.get("Senza categoria").intValue());

        //INSERT INTO spesa (desrizione, prezzo, categoria) VALUES('Terra bruciata', 5990, id_senza_categoria)
        records[5] = new ContentValues();
        records[5].put( FIELD_DESCRIZIONE, "Terra bruciata");
        records[5].put( FIELD_PREZZO, 5990);
        records[5].put( FIELD_CATEGORIA, id.get("Senza categoria").intValue());

        popolaTabella( db, TABLE_SPESA, records );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        System.out.println( "### Cambio di versione del DB ###" );
        System.out.println( "## Inizio aggiornamento..." );

        if ( oldVersion < 2 ) {

        /* creo la nuova tabella 'categoria' */

            createTable( db, SQL_CREATE_TABLE_CATEGORIA );
            System.out.println( "\tTabella '" + TABLE_CATEGORIA + "' creata con successo;" );

        /* popolo la tabella categoria */

            ContentValues[] records = new ContentValues[2];

            //INSERT INTO categoria (nome) VALUES('Senza categoria')
            records[0] = new ContentValues(); records[0].put( FIELD_NOME, "Senza categoria" );

            //INSERT INTO categoria (nome) VALUES('Casa')
            records[1] = new ContentValues(); records[1].put( FIELD_NOME, "Casa" );

            HashMap<String, Long> id = popolaTabella( db, TABLE_CATEGORIA, records );

            System.out.println( "\tInseriti " + records.length + " record in '" + TABLE_CATEGORIA + "';");

        /* aggiungo la colonna categoria alla tabella spesa */
            //ALTER TABLE spesa ADD COLUMN categoria INTEGER
            final String sql_alterTable = "ALTER TABLE " + TABLE_SPESA
                    + " ADD COLUMN " + FIELD_CATEGORIA + " INTEGER";

            db.execSQL( sql_alterTable );

            System.out.println( "\tTabella '" + TABLE_SPESA + "' alterata con successo;" );

        /* per tutti i record già esistenti della tabella spesa imposto il nuovo campo con l'id della
           categoria 'Senza categoria' */

            /*//SELECT _id FROM categoria WHERE nome='Senza categoria'
            final String sql_selectIdCategoria = "SELECT " + FIELD_ID_TABLE_CATEGORIA + " FROM "
                    + TABLE_CATEGORIA + " WHERE " + FIELD_NOME + "=?";

            Cursor cursor = db.rawQuery( sql_selectIdCategoria, new String[]{ "Senza categoria" } );

            cursor.moveToFirst();
            int id_categoria = cursor.getInt(0);
            cursor.close();*/

            //UPDATE spesa SET categoria = 'categoria')
            ContentValues contentValues = new ContentValues();
            contentValues.put( FIELD_CATEGORIA, id.get( "Senza categoria" ).intValue() );

            db.update( TABLE_SPESA, contentValues, null, null );
            System.out.println( "\tAggiornamento dei nuovi campi di '" + TABLE_SPESA + "' completato;" );

        /* aggiungo due voci alla tabella spesa */

            /*records = new ContentValues[2];

            //INSERT INTO spesa (desrizione, prezzo, categoria) VALUES('Lavatrice', 29900, id_casa)
            records[0] = new ContentValues();
            records[0].put( FIELD_DESCRIZIONE, "Lavatrice");
            records[0].put( FIELD_PREZZO, 29900);
            records[0].put( FIELD_CATEGORIA, id.get( "Casa" ).intValue() );

            //INSERT INTO spesa (desrizione, prezzo, categoria) VALUES('Aria fritta', 2950, id_senza_categoria)
            records[1] = new ContentValues();
            records[1].put( FIELD_DESCRIZIONE, "Aria fritta");
            records[1].put( FIELD_PREZZO, 2950);
            records[1].put( FIELD_CATEGORIA, id.get( "Senza categoria" ).intValue() );

            popolaTabella( db, TABLE_SPESA, records );*/

        }
        if ( oldVersion < 3 ) {

        /* aggiungo nuove categorie alla tabella 'categoria' */

            ContentValues[] records = new ContentValues[3];

            //INSERT INTO categoria (nome) VALUES('Cultura')
            records[0] = new ContentValues(); records[0].put( FIELD_NOME, "Cultura" );

            //INSERT INTO categoria (nome) VALUES('Salute')
            records[1] = new ContentValues(); records[1].put( FIELD_NOME, "Salute" );

            //INSERT INTO categoria (nome) VALUES('Tempo libero')
            records[2] = new ContentValues(); records[2].put( FIELD_NOME, "Tempo libero" );

            HashMap<String, Long> id = popolaTabella( db, TABLE_CATEGORIA, records );

            System.out.println( "\tInseriti " + records.length + " record in '" + TABLE_CATEGORIA + "';");

        /* aggiungo una voce di spesa alla tabella 'spese' */

            /*//SELECT _id FROM categoria WHERE nome='Tempo libero'
            Cursor cursor = db.query( TABLE_CATEGORIA,
                    new String[]{FIELD_ID_TABLE_CATEGORIA},
                    FIELD_NOME+"=?",
                    new String[]{"Tempo libero"},
                    null, null, null);
            cursor.moveToFirst();
            int id_tempo_libero = cursor.getInt(0);
            cursor.close();*/

            //INSERT INTO spesa (desrizione, prezzo, categoria) VALUES('Corso android', 0, id_tempo_libero)
            records = new ContentValues[1];
            records[0] = new ContentValues();
            records[0].put( FIELD_DESCRIZIONE, "Fuoco di paglia");
            records[0].put( FIELD_PREZZO, 3860);
            records[0].put( FIELD_CATEGORIA, id.get("Tempo libero").intValue());

            popolaTabella( db, TABLE_SPESA, records );

            System.out.println( "\tInseriti " + records.length + " record in '" + TABLE_SPESA + "';");
        }

        /*if (oldVersion != newVersion) {

            if ( newVersion == 2 ) {
                System.out.println("Inizio aggiornamento del database - Versione 2");

                try {
                    // creo la nuova tabella categoria

                    final String sqlString_createTableCategory = "CREATE TABLE " + TABLE_CATEGORIA
                            + " ("
                            + FIELD_ID_TABLE_CATEGORIA + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                            + FIELD_NOME + " TEXT"
                            + ")";
                    System.out.println( sqlString_createTableCategory );

                    db.execSQL( sqlString_createTableCategory );
                    System.out.println("Tabella 'categoria' creata con successo");

                    // inserisco due record nella tabella 'categoria' per comodità

                    //INSERT INTO categoria (nome) VALUES('Senza categoria')
                    ContentValues contentValues = new ContentValues();
                    contentValues.put( FIELD_NOME, "Senza categoria" );

                    long row = db.insert( TABLE_CATEGORIA, null, contentValues );
                    System.out.println( "Inserito record " + row );

                    //INSERT INTO categoria (nome) VALUES('Casa')
                    contentValues.put( FIELD_NOME, "Casa" );

                    row = db.insert( TABLE_CATEGORIA, null, contentValues );
                    System.out.println( "Inserito record " + row );

                    //INSERT INTO categoria (nome) VALUES('Tempo libero')
                    contentValues.put( FIELD_NOME, "Tempo libero" );

                    row = db.insert( TABLE_CATEGORIA, null, contentValues );
                    System.out.println( "Inserito record " + row );

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                try {

                    // aggiungo la colonna id_categoria alla tabella spesa
                    final String sql_alterTable = "ALTER TABLE " + TABLE_SPESA
                            + " ADD COLUMN " + FIELD_CATEGORIA + " INTEGER";
                    System.out.println( sql_alterTable );

                    db.execSQL( sql_alterTable );
                    System.out.println( "Tabella alterata con successo" );

                    // recupero l'id della categoria 'Senza categoria'
                    //SELECT _id FROM categoria WHERE nome='Senza categoria'
                    final String sql_selectIdCategoria = "SELECT " + FIELD_ID_TABLE_CATEGORIA + " FROM "
                            + TABLE_CATEGORIA + " WHERE " + FIELD_NOME + "=?";

                    final String[] selectionArgs = new String[]{ "Senza categoria" };

                    Cursor cursor = db.rawQuery( sql_selectIdCategoria, selectionArgs );

                    cursor.moveToFirst();
                    int id_categoria = cursor.getInt(0);
                    cursor.close();
                    System.out.println( "ID del record richiesto: " + id_categoria );

                    // imposto il valore del nuovo campo a 'Senza categoria' per tutti i record già presenti nella tabella
                    //"UPDATE spesa SET id_categoria = 'id_categoria')";

                    ContentValues contentValues = new ContentValues();
                    contentValues.put( FIELD_CATEGORIA, id_categoria );

                    long row = db.update( TABLE_SPESA, contentValues, null, null );
                    System.out.println( row + " record modificati" );

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }*/
        System.out.println( "## Aggiornamento completato." );
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //super.onDowngrade(db, oldVersion, newVersion);
    }

    /**
     * crea la tabella specificata usando la query <i>sql</i> sul database <i>db</i>.
     * @param db connessione al database
     * @param sql query contenente lo statement CREATE TABLE
     */
    private static void createTable(SQLiteDatabase db, String sql ) {
        System.out.println( sql );
        db.execSQL( sql );
    }

    /**
     * inserisce nella tabella le righe passate per parametro e restituisce una
     * {@link java.util.HashMap HashMap} con coppie del tipo (nome_categoria, id_categoria)
     * @param db il database su cui operare
     * @param tabella la tabella su cui inserire i nuovi record
     * @param righe le nuove righe da inserire
     * @return  {@link java.util.HashMap HashMap} (nome_categoria, id_categoria)
     */
    private static HashMap<String, Long> popolaTabella(SQLiteDatabase db, String tabella, ContentValues[] righe ) {
        HashMap<String, Long> map = new HashMap<>();
        for ( ContentValues riga : righe ) {
            map.put( riga.getAsString( FIELD_NOME ), db.insert( tabella, null, riga )  );
        }
        return map;
    }
}
