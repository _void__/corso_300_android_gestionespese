package com.example.sw300.gestionespese;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    static final String TOTALE = "Totale €";
    static final String SIMBOLO_VALUTA = "€";

    EditText et_descrizione;
    EditText et_spesa;
    Spinner sp_categoria = null;
    Button btn_inserisci;
    TextView tv_totale;
    ListView lv_spese;

    CursorAdapter spinnerCursorAdapter = null;
    CursorAdapter cursorAdapter = null;

    DBManager db = null;

    private int categoriaSelezionata = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_descrizione = (EditText) findViewById(R.id.et_descrizione);
        et_spesa = (EditText) findViewById(R.id.et_spesa);
        sp_categoria = (Spinner) findViewById(R.id.category_spinner);
        btn_inserisci = (Button) findViewById(R.id.btn_inserisci);
        tv_totale = (TextView) findViewById(R.id.tv_totale);
        lv_spese = (ListView) findViewById(R.id.lv_spese);

        // creo un istanza del controller (MVC) del db */
        db = new DBManager(MainActivity.this);

        Cursor cursor;

    ////////////////////////////////////////////////////////////////////////////////////////////////
        /* (a seguire) gestione dello spinner per la scelta delle categorie tramite CursorAdapter*//*
    ////////////////////////////////////////////////////////////////////////////////////////////////
        cursor = db.fetchCategorie();

        spinnerCursorAdapter = new CursorAdapter( this, cursor, 0 ) {

            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {

                //System.out.println( "Dentro newView() di spinnerCursorAdapter" );

                View view = LayoutInflater.from( context ).inflate( R.layout.spinner_row_layout, null );

                SpinnerHolder spinnerHolder = new SpinnerHolder();
                spinnerHolder.categoria = (TextView) view.findViewById( R.id.sp_row_category_textview);
                view.setTag(spinnerHolder);

                return view;
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {

                //System.out.println( "Dentro bindView() di spinerCursorAdapter" );

                SpinnerHolder spinnerHolder = (SpinnerHolder) view.getTag();
                TextView categoria = spinnerHolder.categoria;
                //int index = cursor.getColumnIndex(DBStrings.FIELD_NOME);
                categoria.setText( cursor.getString( cursor.getColumnIndex( DBStrings.FIELD_NOME ) ) );

                *//* inserisco nel ViewHolder dello spinner l'_id della categoria *//*
                spinnerHolder.id_categoria = cursor.getInt( cursor.getColumnIndex( DBStrings.FIELD_ID_TABLE_CATEGORIA ) );
                //categoria.setTag(  );
            }
        };

        sp_categoria.setAdapter( spinnerCursorAdapter );
    /// CursorAdapter - FINE ///////////////////////////////////////////////////////////////////////*/

    /*
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // (a seguire) gestione dello spinner per la scelta delle categorie con SimpleCursorAdapter
    ////////////////////////////////////////////////////////////////////////////////////////////////
        String[] from = new String[]{ DBStrings.FIELD_NOME };
        int[] to = new int[]{R.id.sp_row_category_textview};

        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter( this, R.layout.spinner_row_layout, cursor, from, to, 0);
        sp_categoria.setAdapter( simpleCursorAdapter );

    // SimpleCursorAdapter - FINE //////////////////////////////////////////////////////////////////*/


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // (a seguire) gestione dello spinner per la scelta delle categorie con SpinnerCategoryAdapter
    ////////////////////////////////////////////////////////////////////////////////////////////////
        final ArrayList<Categoria> categorie = db.fetchCategoriesAsList();
        SpinnerCategoryAdapter spinnerCategoryAdapter =
                new SpinnerCategoryAdapter(this, categorie, "scegli la categoria...");

        sp_categoria.setAdapter(spinnerCategoryAdapter);
    /// SpinnerCategoryAdapter - FINE //////////////////////////////////////////////////////////////


        sp_categoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View spinner_row_layout, int position, long id) {

                // IMPORTANTE - senza questo controllo si blocca l'app quando si ruota il device
                if ( spinner_row_layout != null ) {

                    /////////////////////////////////////////////////////
                    // da usare con CursorAdapter
                    /////////////////////////////////////////////////////
                    /*SpinnerHolder spinnerHolder = (SpinnerHolder) spinner_row_layout.getTag();

                    categoriaSelezionata = spinnerHolder.id_categoria;*/
                    //---------------------------------------------------

                    /////////////////////////////////////////////////////
                    // da usare con SimpleCursorAdapter
                    /////////////////////////////////////////////////////
                    //TextView spinner_row_textview = (TextView) spinner_row_layout.findViewById( R.id.sp_row_category_textview );
                    //---------------------------------------------------

                    /////////////////////////////////////////////////////
                    // da usare con SpinnerCategoyAdapter
                    /////////////////////////////////////////////////////
                    /*SpinnerCategoryAdapter.ViewHolder vh = (SpinnerCategoryAdapter.ViewHolder) spinner_row_layout.getTag();
                    TextView spinner_row_textview = vh.textview;*/
                    TextView spinner_row_textview = ((SpinnerCategoryAdapter.ViewHolder) spinner_row_layout.getTag()).textview;
                    //---------------------------------------------------

                    categoriaSelezionata = (int) spinner_row_textview.getTag();
                    //System.out.println("ID categoria selezionato dallo spinner: " + categoriaSelezionata);
                } else {
                    categoriaSelezionata = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /* mostro la spesa totale */
        aggiornaTotale();

        /* (a seguire) gestione della listView delegata alla visualizzazione della lista della acquisti */
        cursor = db.fetchSpese();

        cursorAdapter = new CursorAdapter(this, cursor, 0) {

            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {

                View view = LayoutInflater.from(context).inflate(R.layout.lv_row_layout, null);

                ListViewHolder listviewHolder = new ListViewHolder();
                listviewHolder.descrizione = (TextView) view.findViewById(R.id.tv_descrizione);
                listviewHolder.categoria = (TextView) view.findViewById(R.id.tv_categoria_listview);
                listviewHolder.prezzo = (TextView) view.findViewById(R.id.tv_prezzo);

                view.setTag(listviewHolder);

                return view;
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {

                ListViewHolder listviewHolder = (ListViewHolder) view.getTag();

                TextView descrizione = listviewHolder.descrizione;
                TextView prezzo = listviewHolder.prezzo;
                TextView categoria = listviewHolder.categoria;

                String desc = cursor.getString(cursor.getColumnIndex(DBStrings.FIELD_DESCRIZIONE));
                int price = cursor.getInt(cursor.getColumnIndex(DBStrings.FIELD_PREZZO));
                int id_categoria = cursor.getInt(cursor.getColumnIndex(DBStrings.FIELD_CATEGORIA));
                String cat = db.findCategoryById(id_categoria, null);

                //int id = cursor.getInt(cursor.getColumnIndex(DBStrings.FIELD_ID_TABLE_SPESA));

                descrizione.setText(desc);
                String formatted_price = ValutaFormatter.formattaValutaPerView(price, MainActivity.SIMBOLO_VALUTA);
                prezzo.setText(formatted_price);
                categoria.setText(cat);
            }
        };

        lv_spese.setAdapter(cursorAdapter);
    }

    /* onCreateOptionMenu viene eseguito la prima volta che l'utente tocca il cab menù */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        /* uso l'inflater per collegare il layout del menu, con l'oggetto menu creato dal sistema */
        getMenuInflater().inflate(R.menu.main_menu, menu);

        /* per visualizzare le voci di menù devo restituire true */
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.cancel_all_action:

                cancellaListaAcquisti();

                /* impongo il refresh della listView */
                cursorAdapter.changeCursor(db.fetchSpese());

                break;
            case R.id.partials_action:
                mostraDettaglioCategorie(); /* avvio una nuova activity */
        }
        return true;
    }

    /**
     * Memorizza in forma permanente
     *
     * @param v
     */
    public void inserisci(View v) {

        String descrizione = et_descrizione.getText().toString();
        String spesa = et_spesa.getText().toString();

        if (descrizione.length() == 0 && spesa.length() == 0) {
            Toast.makeText(this, "Inserire una descrizione e un importo", Toast.LENGTH_LONG).show();
        } else if (spesa.length() == 0) {
            Toast.makeText(this, "Inserire un importo", Toast.LENGTH_SHORT).show();
        } else if (descrizione.length() == 0) {
            Toast.makeText(this, "Inserire una descrizione", Toast.LENGTH_SHORT).show();
        } else if (categoriaSelezionata == -1) {
            Toast.makeText(this, "Selezionare una categoria", Toast.LENGTH_SHORT).show();
        } else {

            long rowId = 0;

            try {
                int spesa_formattata = ValutaFormatter.formattaValutaPerDatabase(spesa, SIMBOLO_VALUTA);
                rowId = db.aggiungiAcquisto(descrizione, spesa_formattata, categoriaSelezionata);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }

            if (rowId == -1) {
                Toast.makeText(MainActivity.this, "Inserimento fallito", Toast.LENGTH_SHORT).show();
            } else {
                cursorAdapter.changeCursor(db.fetchSpese());
                Toast.makeText(MainActivity.this, "Acquisto inserito con successo", Toast.LENGTH_SHORT).show();

                aggiornaTotale();
                clearText();
                categoriaSelezionata = -1;
                sp_categoria.setSelection(0);
            }
        }
    }

    /**
     * Pulisce i campi EditText
     */
    private void clearText() {
        et_descrizione.setText("");
        et_spesa.setText("");
    }

    /**
     * trasforma una stringa rappresentante un prezzo espresso in euro in un intero
     * memorizzabile nel DB.<br/>ES: <code>"13,99€" -> 1399</code>
     *
     * @param prezzo il prezzo in euro da trasformare in int
     * @return l'int corrispondente al prezzo
     */
    private int formattaValutaPerDatabase(String prezzo) {
        Double d = Double.parseDouble(prezzo.replace(SIMBOLO_VALUTA, "").replace(",", ".").trim()) * 100;
        return d.intValue();
// //////////////////////////////////////
//        System.out.println( "prezzo: " + prezzo );
//        String str = prezzo.replace( "€", "" );
//        String str2 = str.replace( ",", "." ).trim();
//        System.out.println( "prezzo dopo trim: " + str2 );
//        Double d = Double.parseDouble( str2 );
//        System.out.println( "prezzo dopo trasformazione in double: " + d );
//        d *= 100;
//        System.out.println( "prezzo dopo moltiplicaione: " + d );
//        int i = d.intValue();
//        System.out.println("prezzo trasformazione in int: " + i);
//        return i;
// ////////////////////////////////////
    }

    /**
     * trasforma l'intero rappresentante un prezzo in una stringa rappresentante lo stesso prezzo espresso in euro.<br/>
     * ES: <code>1399 -> "€13,99"</code>
     *
     * @param prezzo l'intero da trasformare in stringa
     * @return la stringa contenente il prezzo
     */
    private String formattaValutaPerView(int prezzo) {
        return SIMBOLO_VALUTA + String.format("%.2f", prezzo / 100.0);
// ///////////////////////////////
//        System.out.println( "prezzo preso dal db: " + prezzo );
//        double d = prezzo / 100.0;
//        System.out.println( "prezzo dopo divisione: " + d );
//        String s = String.valueOf( d );
//        System.out.println( "prezzo dopo trasformazione in stringa:" + s );
//        return "€" + s;
// ///////////////////////////////
    }

    /**
     * aggiorna il totale visualizzato nella TextView tv_totale.
     * Va usato solo dopo aver inizializzato l'oggetto DBManager db.
     */
    private void aggiornaTotale() {
        String totale = TOTALE + String.format("%.2f", db.calcolaTotale() / 100.0);
        tv_totale.setText(totale);
    }

    /**
     * Cancella l'intera lista
     */
    private void cancellaListaAcquisti() {

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);

        alertBuilder.setTitle("Attenzione")
                .setMessage("Stai per cancellare l'intera lista.\nQuesta operazione è irreversibile.\nVuoi continuare?")
                .setCancelable(false);

        alertBuilder.setPositiveButton("Continua", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.cancellaTuttiRecord();
                Toast.makeText(MainActivity.this, "Cancellazione completata", Toast.LENGTH_LONG).show();
            }

        });

        alertBuilder.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();

    }

    /**
     * Avvia una nuova activity e mostra dettagli delle categorie
     */
    private void mostraDettaglioCategorie() {

        Intent intent = new Intent(this, CategoriesDetailActivity.class);
        startActivity(intent);
    }

    private class ListViewHolder {
        TextView descrizione;
        TextView categoria;
        TextView prezzo;
    }

    private class SpinnerHolder {
        TextView categoria;
        int id_categoria = -1;
    }

}
