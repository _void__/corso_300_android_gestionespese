package com.example.sw300.gestionespese;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.sw300.gestionespese.DBStrings.*;

/**
 * Created by _void__ on 12/05/16.
 */
public class DBManager {

    //private Context ctx = null;
    private DBHelper helper = null;
    private SQLiteDatabase db = null;

    public DBManager(Context c) {
        //this.ctx = c;
        helper = new DBHelper(c, DB_NAME, null, DB_VER);
    }

    /**
     * Memorizza i dati di un acquisto nella lista delle spese
     * @param prodotto il nome del prodotto da aggiungere alla lista
     * @param spesa il prezzo del prodotto.<br/>
     *              Il prezzo deve essere convertito in un <i>int</i> prima di essere passato al metodo.<br/>
     *              ES: se il prezzo di un prodotto è di €13,99 sarà necesario moltiplicare per 100 e poi
     *              passare il risultato al metodo:<br/>
     *              <code>13,99 * 100 = 1399</code>
     * @return l'ID della riga del DB appena creata
     */
    public long aggiungiAcquisto( String prodotto, int spesa, int categoria ) {

        //INSERT INTO spesa VALUES( NULL, 'Latte', 124);

        ContentValues content = new ContentValues();
        content.put( FIELD_DESCRIZIONE, prodotto );
        content.put( FIELD_PREZZO, spesa );
        content.put( FIELD_CATEGORIA, categoria );

        long row = -1;

        try {

            db = helper.getWritableDatabase();
            row = db.insert(TABLE_SPESA,null, content);

        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return row;
    }

    /**
     * fornisce la lista delle spese comprensiva di descrizone e prezzo
     * @return il cursore alla lista delle spese
     */
    public Cursor fetchSpese() {

        SQLiteDatabase db = helper.getReadableDatabase();
        return db.query( TABLE_SPESA, null, null, null, null, null, null );
    }

    /**
     * fornisce la lista delle categorie memorizzata nel db
     * @return il cursore per scorrere i risultati
     */
    public Cursor fetchCategorie() {
        //SELECT * FROM categoria;
        SQLiteDatabase db = helper.getReadableDatabase();
        try {
            return db.query( TABLE_CATEGORIA, null, null, null, null, null, null );
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * fornisce il totale della lista delle spese
     * @return il totale in formato double
     */
    public double calcolaTotale() {
        // Secondo la documentazione di SQLite, total(X) è una funzione non appartenente alle specifiche SQL,
        // che diversamente da sum(x) restituisce sempre un float.
        String sql = "SELECT TOTAL(" + FIELD_PREZZO + ") FROM " + TABLE_SPESA;

        double result = 0.00;

        try {
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery( sql, new String[0] );

            // controllo che il cursore non sia null e che contenga almeno un record
            if ( cursor != null && cursor.getCount() > 0 ) {

                cursor.moveToFirst();
                result = cursor.getDouble( 0 );
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return result;
// /////////////////////////
//        //String sql = "SELECT SUM(" + FIELD_PREZZO + ") FROM " + TABLE_SPESA;
//        String sql = "SELECT SUM(?) FROM ?";
//        db = helper.getReadableDatabase();
//        Cursor cursor = db.rawQuery( sql, new String[]{FIELD_PREZZO, TABLE_SPESA} );
//
//        int result = 0;
//
//        try { // la query restituisce un campo contenente un intero o null
//            result = cursor.getInt(0);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return result;
// /////////////////////////
    }

    /**
     * Elimina tutti i record dalla lista delle spese
     */
    public void cancellaTuttiRecord() {

        try {
            db = helper.getWritableDatabase();
            db.delete( TABLE_SPESA, null, null );
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    /**
     * Cerca e restituisce il nome della categoria in base all'id fornito
     * @param id id della categoria di cui si vuole il nome
     * @param dbConnection usato per riciclare una connessione al db già aperta (in lettura).<br/>
     *                     Se null, una connessione al db viene aperta e poi chiusa all'interno del metodo stesso.
     * @return il nome della categoria
     */
    public String findCategoryById( int id, SQLiteDatabase dbConnection ) {

        String result = "";

        try {
            // se è stato fornito un riferimento ad una connessione già aperta, ne faccio uso,
            // altrimenti ne creo una nuova
            if ( dbConnection == null ) {
                db = helper.getReadableDatabase();
            } else {
                db = dbConnection;
            }

            Cursor cursor = db.query( TABLE_CATEGORIA,
                    new String[]{ FIELD_NOME},
                    FIELD_ID_TABLE_CATEGORIA + "=?",
                    new String[] { String.valueOf(id) },
                    null, null, null );

            cursor.moveToFirst();

            result = cursor.getString(0);
            cursor.close();

        } catch (SQLiteException e) {
            e.printStackTrace();

        } finally {
            // se la connessione al db è stata aperta all'interno del metodo, allora la chiudo.
            // Se è stata passata come paramtro la lascio aperta
            if ( dbConnection == null ) {
                db.close();
            }
        }

        return result;
    }

    /**
     * Cerca e restituisce l'_id della categoria in base al nome specificato
     * @param categoryName nome della categoria di cui si vuole l'_id
     * @param dbConnection usato per riciclare una connessione al db già aperta (in lettura).<br/>
     *                     Se null, una connessione al db viene aperta e poi chiusa all'interno del metodo stesso.
     * @return l'ID della categoria in formato int
     */
    public int findCategoryIdByName(String categoryName, @Nullable SQLiteDatabase dbConnection) {
        //SELECT categoria._id FROM categoria WHERE categoria.nome='categoryName';
        int result = -1;

        try {
            // se è stato fornito un riferimento ad una connessione già aperta, ne faccio uso,
            // altrimenti ne creo una nuova
            if ( dbConnection == null ) {
                db = helper.getReadableDatabase();
            } else {
                db = dbConnection;
            }
            Cursor cursor = db.query( TABLE_CATEGORIA,
                    new String[]{ FIELD_ID_TABLE_CATEGORIA},
                    FIELD_NOME + "=?",
                    new String[] { categoryName },
                    null, null, null );

            cursor.moveToFirst();

            result = cursor.getInt(0);
            cursor.close();

        } catch (SQLiteException e) {
            e.printStackTrace();

        } finally {
            // se la connessione al db è stata aperta all'interno del metodo, allora la chiudo.
            // Se è stata passata come paramtro la lascio aperta
            if ( dbConnection == null ) {
                db.close();
            }
        }

        return result;
    }

    /**
     * fornisce la lista dei nomi delle categorie registrate nel db
     * @return lista delle categorie 
     */
    public ArrayList<String> getNomiCategorie() {
        ArrayList<String> result = new ArrayList<>();

        try {

            db = helper.getReadableDatabase();
            // SELECT nome FROM categoria;
            Cursor cursor = db.query( TABLE_CATEGORIA,
                    new String[]{ FIELD_NOME},
                    null,
                    null,
                    null, null, null );

            cursor.moveToFirst();

            int i = 0;
            while ( !cursor.isAfterLast() ) {
                result.add( cursor.getString(i) );
                i++;
            }

            //cursor.close();

        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return result;
    }
    
    public ArrayList<Categoria> fetchCategoriesAsList() {
        ArrayList<Categoria> result = new ArrayList<>();
        
        Cursor cursor = this.fetchCategorie();
        //cursor.moveToFirst();

        while ( cursor.moveToNext() ) {
            Categoria cat = new Categoria();
            cat.id = cursor.getInt( cursor.getColumnIndexOrThrow( FIELD_ID_TABLE_CATEGORIA) );
            cat.nome = cursor.getString( cursor.getColumnIndexOrThrow( FIELD_NOME) );
            //cursor.moveToNext();
            result.add(cat);
        }

        return result;
    }

    /**
     * fornisce il totale relativo alla categoria scelta.<br/>
     * Il totale è restituito sotto forma di intero, dovrà quindi essere covertito in double.<br/>
     * ES: 12050 -> 12050/100.00 -> €120.50
     * @param categoria il nome della categoria di cui si vuole la somma parziale
     * @return intero reppresentante il totale
     */
    public int calcolaTotalePerCategoria( String categoria ) {

        int result = 0;

        //SELECT SUM(spesa.prezzo) FROM spesa WHERE spesa.categoria=?;
        String sql = "SELECT SUM(" + TABLE_SPESA + "." + FIELD_PREZZO + ") "
                + "FROM " + TABLE_SPESA
                + " WHERE " + TABLE_SPESA + "." + FIELD_CATEGORIA + "=?";

        try {

            db = helper.getReadableDatabase();

            int id_categoria = findCategoryIdByName( categoria, db );

            Cursor cursor = db.rawQuery( sql, new String[]{ String.valueOf( id_categoria ) } );
            cursor.moveToFirst();

            result = cursor.getInt(0);
            //cursor.close();

        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return result;
        /*//SELECT SUM(spesa.prezzo) FROM spesa JOIN categoria ON spesa.id_categoria=categoria._id
        // WHERE categoria._id=?;
        String sql = "SELECT SUM(" + TABLE_SPESA + "." + FIELD_PREZZO + ") "
                    + "FROM " + TABLE_SPESA + " JOIN " + TABLE_CATEGORIA
                    + " ON "+ TABLE_SPESA + "." + FIELD_CATEGORIA
                    + "=" + TABLE_CATEGORIA + "." + FIELD_ID_TABLE_CATEGORIA
                    + " WHERE " + TABLE_CATEGORIA + "." + FIELD_NOME + "=?";
        System.out.println(sql);
        try {
            db = helper.getReadableDatabase();
            return db.rawQuery( sql, new String[]{});
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }*/
    }

    public Map<String, Integer> calcolaTotaliCategorie() {

        ArrayList<String> categorie = getNomiCategorie();
        Map<String, Integer> totali = new HashMap<>();

        //db = helper.getReadableDatabase();
        for ( String categoria: categorie ) {
            totali.put( categoria, calcolaTotalePerCategoria( categoria ) );
        }

        return totali;
    }
    
    /*public class Categoria {
        
        String nome = "";
        int id = -1;
        
        @Override
        public String toString() {
            return nome;
        }
    }*/
}
