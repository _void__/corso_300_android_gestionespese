package com.example.sw300.gestionespese;

import android.support.annotation.Nullable;

/**
 * Created by Raiden on 01/06/2016.
 */
public class ValutaFormatter {

    /**
     * trasforma una stringa rappresentante un prezzo espresso nella valuta selezionata in un intero
     * memorizzabile nel DB.<br/>ES: <code>"€13,99" -> 1399</code>
     * @param prezzo la stringa rappresentante il prezzo in notazione decimale da trasformare in int
     * @param simboloValuta il simbolo della valuta locale. Se null viene impostato in base alla località
     * @return l'int corrispondente al prezzo
     */
    public static int formattaValutaPerDatabase( String prezzo, @Nullable String simboloValuta ){
        if ( simboloValuta == null ){
            java.util.Currency valuta = java.util.Currency.getInstance( java.util.Locale.getDefault() );
            simboloValuta = valuta.getSymbol();
        }
        Double d = Double.parseDouble( prezzo.replace( simboloValuta, "" ).replace( ",", "." ).trim() ) * 100;
        return d.intValue();
    }

    /**
     * trasforma l'intero rappresentante un prezzo in una stringa rappresentante lo stesso prezzo espresso nella valuta selezionata.<br/>
     * ES: <code>1399 -> "€13,99"</code>
     * @param prezzo l'intero da trasformare in stringa
     * @param simboloValuta il simbolo della valuta locale. Se null viene impostato in base alla località
     * @return la stringa contenente il prezzo
     */
    public static String formattaValutaPerView( int prezzo, @Nullable String simboloValuta ) {
        if ( simboloValuta == null ){
            java.util.Currency valuta = java.util.Currency.getInstance( java.util.Locale.getDefault() );
            simboloValuta = valuta.getSymbol();
        }
        return simboloValuta + String.format( java.util.Locale.getDefault(), "%.2f", prezzo / 100.0 );
    }
}
