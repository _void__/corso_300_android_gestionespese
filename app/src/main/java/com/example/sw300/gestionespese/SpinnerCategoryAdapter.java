package com.example.sw300.gestionespese;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/** Adapter che espone i dati di un {@link java.util.ArrayList ArrayList} ad uno
 * {@link android.widget.Spinner Spinner}.<br/>
 * Created by _void__ on 02/06/2016.
 */
public class SpinnerCategoryAdapter extends BaseAdapter {

    private final Context mContext;
    private ArrayList<Categoria> mCategorie;
    private boolean mPrimoElementoFittizioAttivo;

    /**
     * Costruttore di base della classe
     * {@link com.example.sw300.gestionespese.SpinnerCategoryAdapter SpinnerCategoryAdapter}
     * @param context il contesto attuale
     * @param categorie lista delle categorie da mostrare nello spinner
     */
    public SpinnerCategoryAdapter( Context context, ArrayList<Categoria> categorie ) {
        //super();
        mContext = context;
        mCategorie = categorie;
        mPrimoElementoFittizioAttivo = false;
    }

    /**
     * Costruttore che permette di definire un primo elemento fittizio nello spinner. <br/>
     * L'id dell'elemento viene impostato a -1.
     * @param context il contesto attuale
     * @param categorie lista delle categorie da mostrare nello spinner
     * @param primoElemento testo del primo elemento da mostrare nello spinner. <br/>
     *                      Verrà mostrato con l'aspetto grafico di una <i>hint</i>
     */
    public SpinnerCategoryAdapter( Context context, ArrayList<Categoria> categorie, String primoElemento ) {

        mContext = context;
        mCategorie = categorie;
        mPrimoElementoFittizioAttivo = true;

        Categoria segnaposto = new Categoria();
        segnaposto.nome = primoElemento;
        segnaposto.id = -1;

        categorie.add(0, segnaposto);
    }

    @Override
    public int getCount() {
        return mCategorie.size();
    }

    @Override
    public Object getItem(int position) {
        return mCategorie.get( position );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View spinner_row_layout, ViewGroup parent) {

        ViewHolder vh_spinner_row_layout;

        if ( spinner_row_layout == null ){

            spinner_row_layout = LayoutInflater.from( mContext ).inflate( R.layout.spinner_row_layout, null );

            vh_spinner_row_layout = new ViewHolder();
            vh_spinner_row_layout.textview =  (TextView) spinner_row_layout.findViewById( R.id.sp_row_category_textview );

            spinner_row_layout.setTag( vh_spinner_row_layout );

        } else {

            vh_spinner_row_layout = (ViewHolder) spinner_row_layout.getTag();
        }
        /* prendo il riferimento alla textview corrente */
        TextView nome_categoria = vh_spinner_row_layout.textview;


        /*if ( mPrimoElementoFittizioAttivo ) {

            if ( position == 0) {
                *//* se sto usando l'elemento fittizio per la prima voce del menu e sto disegnado la
                   sua view corrispondente (position == 0), imposto il testo come una hint *//*
                nome_categoria.setHint(mCategorie.get(position).nome);
                nome_categoria.setText( "" );

            } else {
                *//* se sto usando l'elemento fittizio per la prima voce del menu ma sto disegnado le
                   vere categorie (position != 0), imposto il testo normalmente *//*
                nome_categoria.setText( mCategorie.get( position ).nome );
            }
        } else {
            *//* se non sto usando l'elemento fittizio per la prima voce di menu, imposto il
               testo normalmente *//*
            nome_categoria.setText( mCategorie.get( position ).nome );
        }

        nome_categoria.setTag( mCategorie.get( position ).id );*/

        /* prendo la categoria in posizione position nella lista delle categorie e ne leggo l'id */
        int id = mCategorie.get( position ).id;

        if ( id == -1 ) {
            /* se sto disegnando l'elemento fittizio, ne modifico l'aspetto */
            nome_categoria.setHint( mCategorie.get( position ).nome );
            nome_categoria.setText( "" );
            nome_categoria.setTag( id );
        } else {
            /* sto disegnando gli elementi reali*/
            nome_categoria.setText( mCategorie.get(position).nome );
            nome_categoria.setTag( id );
            nome_categoria.setHint( "" );
        }

        return spinner_row_layout;
    }

    /**
     * Classe statica di utilità per lo
     * {@link com.example.sw300.gestionespese.SpinnerCategoryAdapter SpinnerCategoryAdapter}
     * e i suoi utilizzatori.<br/>
     * Memorizza il riferimento alla {@link android.widget.TextView TextView} con il nome della categoria
     */
    public class ViewHolder {
        TextView textview;
    }

}
