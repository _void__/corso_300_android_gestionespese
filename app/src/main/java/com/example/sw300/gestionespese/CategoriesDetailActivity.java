package com.example.sw300.gestionespese;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Raiden on 31/05/2016.
 */
public class CategoriesDetailActivity extends AppCompatActivity {

    TextView categoria = null;
    TextView prezzo = null;

    ListView lvDettaglioCategorie = null;

    DBManager mDBManager = null;
    Cursor mCursor = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_categories_detail);

        lvDettaglioCategorie = (ListView)findViewById(R.id.lv_dettaglio_categorie);

        mDBManager = new DBManager(this);

        mCursor = mDBManager.fetchCategorie();

        CursorAdapter cursorAdapter = new CursorAdapter(this, mCursor, 0) {

            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {

                View view = LayoutInflater.from( context ).inflate( R.layout.categories_detail_row, null );

                ViewHolder holder = new ViewHolder();
                holder.categoria = (TextView) view.findViewById( R.id.tv_dettaglio_categoria );
                holder.prezzo = (TextView) view.findViewById( R.id.tv_dettaglio_prezzo );

                view.setTag(holder);

                return view;
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {
                ViewHolder holder = (ViewHolder) view.getTag();

                TextView tvCategoria = holder.categoria;
                TextView tvPrezzo = holder.prezzo;

                int categoriaColumnIndex = cursor.getColumnIndex(DBStrings.FIELD_NOME);

                String categoria = cursor.getString( categoriaColumnIndex );
                tvCategoria.setText(categoria);

                //String totale = new String().valueOf( totale( categoria ) );
                int parziale = totale( categoria );
                tvPrezzo.setText( ValutaFormatter.formattaValutaPerView( parziale, null ) );
            }
        };

        lvDettaglioCategorie.setAdapter(cursorAdapter);

    }


    private int totale( String categoria ) {

        return mDBManager.calcolaTotalePerCategoria( categoria );
    }


    private class ViewHolder {
        TextView categoria = null;
        TextView prezzo = null;
    }
}
